import Signin from './user/signin'
import Signup from './user/signup'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import { BrowserRouter, Routes, Route } from 'react-router-dom'


function App() {
  return (
    <BrowserRouter>
      

      <Routes>
        <Route path='/signin' element={<Signin />} />
        <Route path='/signup' element={<Signup />} />
        
      </Routes>

   
      <ToastContainer position='top-center' autoClose={1000} />
    </BrowserRouter>
  )
}

export default App;
